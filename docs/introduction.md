---
prev: /
next: /api.html
title: Getting started
---

## About

Hitchy's sever development tools - or SDT - is a toolkit for unit-testing applications based on and plugins for [Hitchy](https://core.hitchy.org/). 

It is [exposing methods](api.md#methods) 

* to integrate a running Hitchy instance serving an application in a configurable project folder with test runners like [mocha](https://mochajs.org/), 
* to control that Hitchy instance and 
* to interact with it via local HTTP requests or via its exposed API.

The [project folder](#folders) is [customizable](api.md#projectfolder), gets [auto-detected](#auto-discovering-folders) or is [based on a map of strings describing content of files to write](api.md#files) before starting Hitchy. All options can be combined, too.

Last but not least, a [CLI tool](cli/hitchy-pm.md) helps with temporarily fetching peer dependencies of a plugin you are testing.

## Installation

Always install this package _as a development dependency_:

```bash
npm i -D @hitchy/server-dev-tools
```

:::warning Dependencies of a plugin
A Hitchy plugin should never list [Hitchy's core](https://www.npmjs.com/package/@hitchy/core) as a regular dependency. 

* In versions before v0.6, the core has been declared as a regular dependency of SDT for testing purposes. This was potentially interfering with a dependency declared on the tested plugin.
* In versions since v0.6, fetching a supported version of **@hitchy/core** for testing is up to either plugin again. But as it is used for testing with SDT, only, the dependency should be explicitly declared for development accordingly.

Thus, for production use, your plugin should mark **@hitchy/core** as a _peer dependency_:

```json
{
    "peerDependencies": {
        "@hitchy/core": "*"
    }
}
```

In addition, when using SDT version 0.6 or later, it should be declared as a _development dependency_ for testing purposes:

```json
{
    "devDependencies": {
        "@hitchy/core": "*"
    }
}
```
:::

## Usage

SDT [exposes methods](api.md#methods) for easily starting and stopping an instance of Hitchy in current process. The testing environment can be [configured](api.md#configuration) to evaluate the code in different scenarios.

### Changes in v0.6

Starting with v0.6 SDT's API has changed to be less tightly bound to **@hitchy/core**. Testing-related code previously included with **@hitchy/core** has been moved to SDT. However, parts of that code still require access on **@hitchy/core**. For that, versions 1.0.0 and later of **@hitchy/core** are exposing methods and properties which as an API has been coined the core's SDT API. Because of this dependency, the way of using SDT has changed. See the examples for additional information. 

* [API](api.md)
* [Examples](examples.md)

## Terminology

### Application vs. plugin

SDT is designed to support in two kinds of projects:

* A Hitchy-based _application_ depends on Hitchy's core to expose endpoints and to route client requests to associated handlers. An application is usually installed as-is.

* A _plugin_ for Hitchy extends functionality of Hitchy's core. It can be pulled by an application as another dependency. Thus, it is capable of running in context of different applications probably providing each one with a slightly different functionality.

### Folders

In every project, these distinguishable folders are expected:

* The _project folder_ contains a Hitchy-based application's implementation. It usually consists of folders named **config**, **api/controllers**, **api/policies** etc.

* The _plugins folder_ consists of a **node_modules** sub-folder listing dependencies of your application. It is used by Hitchy to discover plugins.

Both folders are usually identical, but may be different, too. The latter applies to test projects used for testing plugins or to [application projects combining client-side code with server-side code](https://core.hitchy.org/tutorials/vue.html).

:::warning Breaking change of names <Badge text="0.4.0+"></Badge>
In recent versions, the naming of options picking folders has changed several times. However, their latest names have not been as sound as desired, either. By introducing auto-discovery of folders, names have been reduced again, causing some of them returning to their previous versions.
:::

## Auto-discovering folders <Badge text="0.3.0+"></Badge>

SDT is designed to work out of the box in most use cases by automatically discovering [project folder and plugins folder](#folders) unless configured explicitly.

Auto-discovery often depends on folder containing _current test_. This file is the one importing **@hitchy/server-dev-tools** to use exposed methods for starting Hitchy instance.

The process for auto-discovering folders has been refactored in v0.6.2. It is now starting to discover the root folder of your project prior to auto-discovering the project folder as well as a plugins folder.

### Discovering the root folder

When developing an application based on or some plugin for Hitchy, there is a folder you set up as a project. This folder has a **package.json** file and lists @**hitchy/server-dev-tools** as one of its dependencies. 

Discovery is based on the assumption that the current test script's file is contained in that folder or any of its sub-folders. The following algorithm is starting in that folder.

1. Does it have a **package.json** file? If not, skip this folder and check its parent accordingly.

2. Is it listing **@hitchy/server-dev-tools** as its dependency? If not, collect this folder as a _candidate_ and restart for the parent folder.

3. Stop iterating folders, if your project is about implementing a plugin for Hitchy or currently checked folder does not have a **hitchy.json** file.

4. Otherwise, restart for the parent folder.

This approach may result in multiple folders collected as candidates.

* If there is only one candidate, it is used right away. 

* If there are multiple candidates

  * check the most recently found (thus outermost) candidate for declaring [npm workspaces](https://docs.npmjs.com/cli/v7/using-npm/workspaces?v=true) and use the first of the remaining candidates matching any of the declared workspace path names.
  * use that outermost candidate in case there are not workspaces defined or none of the remaining candidates is matching any declared workspace path name.

### Discovering project folder

The [discovered root folder](#discovering-the-root-folder) and its subordinated folders are searched for the one _looking like_ a Hitchy-based application's project folder. Discovery succeeds if a single folder has been found, only.

On searching subordinated folders, some of them are ignored.

:::details Ignores folders
| folder | why? |
|--------|----|
| coverage | containing test coverage reports |
| data | containing data locally managed by **@hitchy/plugin-odem** |
| dist | containing a build of your application |
| node_modules | containing 3rd-party dependencies |
| public | containing data publicly exposed for direct access |
| .git | containing your local git repository |
| .idea | containing configuration of IDEA-based IDEs |
| .vitepress | containing a VitePress-based documentation |
| .vscode | containing configuration of Visual Studio Code |
| .vuepress | containing a VuePress-based documentation |
:::

Any tested folder with one of these sub-folders

* config/
* api/controllers/ or api/controller/
* api/policies/ or api/policy/
* api/models/ or api/model/
* api/services/ or api/service/

deeply containing at least one Javascript file is picked as a candidate. This includes files with extensions **.js**, **.mjs** or **.cjs**, but excludes files with either extension preceded by **.spec** or **.test**.

If multiple folders have been found, a debug message is logged listing all candidates. The auto-discovery has failed in that case and [a project folder must be selected](api.md#projectfolder) explicitly.

:::tip Disabling auto-discovery
The auto-discovery of a project folder can result in unintended matches. This could be an issue on [describing files of a temporary project folder](api.md#files) from scratch. For that reason, auto-discovery can be disabled either

* by [selecting a project folder](api.md#projectfolder) to be used explicitly or
* by providing `false` as selected project folder.
:::

### Discovering plugins folder

When using SDT to develop a plugin for Hitchy, you are expected to set [plugin option](api.md#plugin) in the configuration. This is enabling support for discovering your plugin's code automatically. It starts at the folder with the file containing current test script and processes these steps:

1. Does the current folder contain a **hitchy.json** file? If so, use this folder as a result of discovery.

2. Does it contain a **package.json** file? If so, stop iterating for assuming this is a project's root folder. The discovery has failed in that case.

3. Otherwise restart for the parent folder.
