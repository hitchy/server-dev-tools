import meta from "../../package.json";

export default {
	lang: "en",
	base: "/",
	outDir: "../public",
	title: `Hitchy SDT Manual v${meta.version}`,
/*
	head: [
		[ "link", {
			rel: "icon",
			href: "/favicon.svg",
			type: "image/svg"
		} ],
	],
*/
	themeConfig: {
		sidebar: "auto",
		displayAllHeaders: true,
		repo: "https://gitlab.com/hitchy/server-dev-tools",
		repoLabel: "Contribute!",
		outline: {
			level: [2,4]
		},
		footer: {
			copyright: 'created by <a target="_blank" href="https://cepharum.de/en/">cepharum GmbH</a>',
			message: '<a target="_blank" href="https://cepharum.de/en/imprint.html">Imprint</a>'
		},
		nav: [
			{ text: "Introduction", link: "/introduction.html" },
			{ text: "API", link: "/api.html" },
			{ text: "CLI", link: "/cli/hitchy-pm.html" },
			{ text: "Examples", link: "/examples.html" },
			{ text: "Hitchy", items: [
					{ text: "Core", link: "https://core.hitchy.org/" },
					{ text: "Plugins", items: [
							{ text: "Odem", link: "https://odem.hitchy.org/" },
							{ text: "Auth", link: "https://auth.hitchy.org/" },
						] },
					{ text: "Tools", items: [
							{ text: "SDT", link: "/" },
						] }
				] },
		],
	},
};
