---
prev: /api.html
next: /examples.html
---

## Hitchy package manager <Badge type="info" text="0.1.0+"></Badge>

There is a tool named `hitchy-pm` that can be used in projects to fetch peer dependencies of your project e.g. prior to running unit tests.

It takes a list of peer dependencies and checks whether they exist in current project or not. It is installing all missing dependencies using `npm` with `--no-save` option.

```bash
hitchy-pm @hitchy/plugin-auth @hitchy/plugin-session @hitchy/plugin-cookies @hitchy/plugin-odem
``` 

The tool is qualifying all provided names of peer dependencies, so it's okay to omit the prefix `@hitchy/plugin-` on every listed dependency for sake of readability.

```bash
hitchy-pm auth session cookies odem
```

:::tip
Omitting prefix does not work unless it is exactly matching `@hitchy/plugin-`.
:::

```bash
hitchy-pm auth session cookies odem --exec mocha --recursive --ui bdd test/scripts
```

This line is making sure all listed Hitchy plugins are available prior to invoking mocha for running tests in selected folder.

### Supported arguments

| argument   | description |
|------------|-------------|
| `--quiet`  | Suppresses output not related to some malfunction. |
| `--exec`   | This option ends list of parameters processed by hitchy-pm itself and starts collection of command name and its arguments to be invoked after having installed all missing dependencies. |
| `--resolve` | **Not yet supported.**<br><br>This is going to use smarter approach inspecting current project's configuration and configuration of all its dependencies recursively to find the required peer dependencies. |
