/// <reference types="@hitchy/types" />

import { Server, ServerResponse } from "http";
import * as Types from "@hitchy/types/core";

declare namespace Hitchy.ServerDevTools {
    /**
     * Exposes function for generating methods for controlling tests in context
     * of a running Hitchy instance based on provided core API.
     *
     * @param core least API of @hitchy/core necessary for running a Hitchy instance
     */
    export default function(core?: CoreAPI): API;

    /**
     * Provides methods for controlling tests assessing code in context of a
     * running Hitchy instance.
     */
    export interface API {
        /**
         * Generates asynchronous function for use with a test runner's before()
         * hook.
         *
         * @example
         *     import SDT from "@hitchy/server-dev-tools";
         *     import Core from "@hitchy/core/sdt.js";
         *
         *     const Test = await SDT( Core );
         *
         *     describe( "Test", () => {
         *         const ctx = {};
         *
         *         before( Test.before( ctx, { ... config here ... } ) );
         *         after( Test.after( ctx ) );
         *
         *         it( "is working", () => {
         *             ... your test here ...
         *         } );
         *     });
         *
         * @param ctx object provided to expose properties and methods for interacting with running Hitchy instance
         * @param configuration customizations for the Hitchy instance to run
         * @param timeout timeout in milliseconds to wait for the server to be up and running, defaults to 2000ms
         */
        before( ctx: ContextDescriptor, configuration: Configuration, timeout?: number ): () => Promise<void>;

        /**
         * Generates asynchronous function for use with a test runner's after()
         * hook.
         *
         * @param ctx same object provided on generating function for the test runner's before() hook @see before()
         * @param keepFiles if true, files in a temporary folder are kept (e.g. for investigating issues of a failing test)
         */
        after( ctx: ContextDescriptor, keepFiles?: boolean ): () => Promise<void>;

        /**
         * Starts Hitchy instance with provided customizations.
         *
         * @example
         *     import SDT from "@hitchy/server-dev-tools";
         *     import Core from "@hitchy/core/sdt.js";
         *
         *     const Test = await SDT( Core );
         *
         *     describe( "Test", () => {
         *         let ctx;
         *
         *         before( async() => {
         *             ctx = await Test.start( { ... config here ... } );
         *         } );
         *         after( () => Test.stop( ctx ) );
         *
         *         it( "is working", () => {
         *             ... your test here ...
         *         } );
         *     });
         *
         * @param configuration customizations for the Hitchy instance to run
         * @returns object with properties and methods for interacting with the running Hitchy instance
         */
        start( configuration: Configuration ): Promise<ContextDescriptor>;

        /**
         * Shuts down Hitchy instance started before with @see start() method,
         *
         * @param ctx object returned from @see start() for accessing Hitchy instance to shut down here
         * @returns promise resolved when Hitchy instance has been shut down
         */
        stop( ctx: ContextDescriptor ): Promise<void>;

        /**
         * Sends HTTP request to the running Hitchy instance.
         *
         * @param method HTTP request method
         * @param url request URL consisting of path name and optional query
         * @param body request payload, an object is converted to JSON automatically
         * @param headers custom request headers
         * @returns promise for the Hitchy instance's response
         */
        request( method: string, url: string, body?: Buffer|string|object, headers?: { [ key: string ]: string } ): Promise<ExtendedServerResponse>;
    }

    export interface ContextInformation {
        /** Exposes web server started for receiving requests to be dispatched into Hitchy instance. */
        server: Server;
        /** Exposes instance of Hitchy handling requests received via web server. */
        hitchy: Types.Hitchy.Core.Instance;
        /** Exposes pathname of temporary folder created automatically for compiling temporary project to be processed by Hitchy. */
        temporaryFolder?: string;
        /** Exposes options eventually passed to code for starting Hitchy instance. */
        hitchyOptions: object;
        /** Exposes CLI arguments eventually passed to code for starting Hitchy instance. */
        args: object;
    }

    export type RuntimeContext = ContextInformation | Promise<ContextInformation>;

    export interface ContextDescriptor extends ContextInformation {
        /** Lists messages logged to console. */
        logged?: string[];

        /** Send GET request to running Hitchy application. */
        get: ( url: string, headers?: { [ key: string ]: string } ) => Promise<ExtendedServerResponse>;
        /** Send POST request to running Hitchy application. */
        post: ( url: string, body?: Buffer|string|object, headers?: { [ key: string ]: string } ) => Promise<ExtendedServerResponse>;
        /** Send PUT request to running Hitchy application. */
        put: ( url: string, body?: Buffer|string|object, headers?: { [ key: string ]: string } ) => Promise<ExtendedServerResponse>;
        /** Send PATCH request to running Hitchy application. */
        patch: ( url: string, body?: Buffer|string|object, headers?: { [ key: string ]: string } ) => Promise<ExtendedServerResponse>;
        /** Send DELETE request to running Hitchy application. */
        delete: ( url: string, body?: Buffer|string|object, headers?: { [ key: string ]: string } ) => Promise<ExtendedServerResponse>;
        /** Send HEAD request to running Hitchy application. */
        head: ( url: string, headers?: { [ key: string ]: string } ) => Promise<ExtendedServerResponse>;
        /** Send OPTIONS request to running Hitchy application. */
        options: ( url: string, headers?: { [ key: string ]: string } ) => Promise<ExtendedServerResponse>;
        /** Send TRACE request to running Hitchy application. */
        trace: ( url: string, headers?: { [ key: string ]: string } ) => Promise<ExtendedServerResponse>;

        /** Send HTTP request to running Hitchy application. */
        request: ( method: string, url: string, body?: Buffer|string|object, headers?: { [ key: string ]: string } ) => Promise<ExtendedServerResponse>;
    }

    /**
     * Describes options supported by server-dev-tools on managing Hitchy
     * temporarily run for unit-testing.
     */
    export interface Configuration {
        /** Selects existing folder on disk containing project to be presented by wrapped Hitchy. [default: none] */
        projectFolder?: string;
        /** Selects existing folder on disk containing code of plugin to be tested via some temporary application. [default: none] */
        pluginsFolder?: string;
        /** Explicitly indicates server-dev-tools used for testing a plugin for Hitchy's core. */
        plugin?: boolean;
        /** Provides options to be forwarded to managed instance of Hitchy. [default: none] */
        options?: Types.Hitchy.Core.Options;
        /** Maps pathnames relative to project's root of files to be temporarily created into desired content of either file. [default: none] */
        files?: { [key: string]: string };
        /** Controls whether enforcing to copy selected project folder into temporary one. [default: false] */
        useTmpPath?: boolean;
    }

    export type GeneratedFunction = () => Promise<void>;

    /**
     * Generates function for use with test runners' support for setting up
     * runtime environment for actual test suites.
     *
     * @param context handle of resulting runtime environment, will be populated on invoking returned function
     * @param configuration customizations for runtime environment
     * @param timeout timeout in milliseconds for running returned function
     * @returns function for use with methods like mocha's `before()` or `beforeEach()`
     */
    export function before(context: ContextDescriptor, configuration: Configuration, timeout?: number ): GeneratedFunction;

    /**
     * Generates function for use with test runners' support for tearing down
     * runtime environment after running actual test suites.
     *
     * @param context handle populated on invoking function returned from `before()`
     * @param keepFiles set true to prevent removal of temporarily created project folder
     * @returns function for use with methods like mocha's `before()` or `beforeEach()`
     */
    export function after( context: ContextDescriptor, keepFiles?: boolean ): GeneratedFunction;

    /**
     * Starts instance of Hitchy for providing application customized by provided
     * options.
     *
     * @param configuration options customizing way of setting up project folder to be processed by Hitchy
     * @returns promise for information to access started server and Hitchy instance
     */
    export function start( configuration: Configuration ): Promise<ContextInformation>;

    /**
     * Stops previously started instance of Hitchy.
     *
     * @param context context information returned from calling `start()` before
     * @param keepFiles set true to prevent removal of temporarily created project folder
     * @returns promise Hitchy instance shut down
     */
    export function stop( context: ContextInformation, keepFiles?: boolean ): Promise<void>;

    export interface ExtendedServerResponse extends ServerResponse {
        /** Exposes raw response body. */
        body: Buffer;
        /** Exposes parsed data provided in JSON-formatted response body. */
        data?: any;
        /** Exposes string found in text-type response body. */
        text?: string;
    }

    /**
     * Sends HTTP request to server described by context given as `this` and
     * promises server's response.
     *
     * @param method HTTP method
     * @param url request URL
     * @param body request body
     * @param headers custom request headers
     * @returns promise for server's response including response body probably pre-parsed into text (on text response) or object (on JSON response)
     */
    export function request( this: ContextInformation, method: string, url: string, body?: Buffer|string|object, headers?: { [ key: string ]: string } ): Promise<ExtendedServerResponse>;

    export class AbstractLogger {
        get( namespace: string ): ( message: string, ...args: string ) => void;
        update( switches: string ): void;
    }

    export class CurrentLogger extends AbstractLogger {
        get( namespace: string ): ( message: string, ...args: string ) => void;
        update( switches: string ): void;
    }

    export class CapturingLogger extends AbstractLogger {
        get( namespace: string ): ( message: string, ...args: string ) => void;
        update( switches: string ): void;
    }

    export interface CoreAPI {
        Logger: CurrentLogger;
        AbstractLogger: AbstractLogger;
        CapturingLogger: CapturingLogger;
        LogLevels: string[];
        BasicServer( options: Hitchy.Core.Options, arguments: string[], onShutdownComplete: () => void ): Promise<{
            server: Server,
            hitchy: Hitchy.Core.Instance
        }>;
    }
}
