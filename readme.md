# @hitchy/server-dev-tools [![pipeline status](https://gitlab.com/hitchy/server-dev-tools/badges/master/pipeline.svg)](https://gitlab.com/hitchy/server-dev-tools/-/commits/master)

_toolkit for testing plugins and applications based on [Hitchy](https://www.npmjs.com/package/@hitchy/core)_

## License

[MIT](LICENSE)

## Manual

The full manual is available at [https://sdt.hitchy.org/](https://sdt.hitchy.org/).
