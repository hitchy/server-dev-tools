import { describe, before, after, it } from "mocha";
import "should";

import Core from "@hitchy/core/sdt.js";
import SDT from "../../index.js";

const Test = await SDT( Core );

const config = {
	files: {
		"config/routes.cjs": `exports.routes = { "/foo": ( _, res ) => res.send( "done" ) };`
	}
};

describe( "SDT provides context descriptor which", () => {
	const ctx = {};

	before( Test.before( ctx, config ) );
	after( Test.after( ctx ) );

	it( "provides helper method for sending GET requests to running Hitchy instance", async() => {
		ctx.get.should.be.a.Function();

		( await ctx.get( "/foo" ) ).statusCode.should.equal( 200 );
	} );

	it( "provides helper method for sending POST requests to running Hitchy instance", async() => {
		ctx.post.should.be.a.Function();

		( await ctx.post( "/foo" ) ).statusCode.should.equal( 200 );
	} );

	it( "provides helper method for sending PUT requests to running Hitchy instance", async() => {
		ctx.put.should.be.a.Function();

		( await ctx.put( "/foo" ) ).statusCode.should.equal( 200 );
	} );

	it( "provides helper method for sending PATCH requests to running Hitchy instance", async() => {
		ctx.patch.should.be.a.Function();

		( await ctx.patch( "/foo" ) ).statusCode.should.equal( 200 );
	} );

	it( "provides helper method for sending DELETE requests to running Hitchy instance", async() => {
		ctx.delete.should.be.a.Function();

		( await ctx.delete( "/foo" ) ).statusCode.should.equal( 200 );
	} );

	it( "provides helper method for sending HEAD requests to running Hitchy instance", async() => {
		ctx.head.should.be.a.Function();

		( await ctx.head( "/foo" ) ).statusCode.should.equal( 200 );
	} );

	it( "provides helper method for sending OPTIONS requests to running Hitchy instance", async() => {
		ctx.options.should.be.a.Function();

		( await ctx.options( "/foo" ) ).statusCode.should.equal( 200 );
	} );

	it( "provides helper method for sending TRACE requests to running Hitchy instance", async() => {
		ctx.trace.should.be.a.Function();

		( await ctx.trace( "/foo" ) ).statusCode.should.equal( 200 );
	} );

	it( "provides reference to running Hitchy's API", () => {
		ctx.hitchy.api.should.be.an.Object();

		ctx.hitchy.api.controllers.should.be.an.Object();
		ctx.hitchy.api.policies.should.be.an.Object();
		ctx.hitchy.api.services.should.be.an.Object();
		ctx.hitchy.api.models.should.be.an.Object();

		ctx.hitchy.api.services.ControllerRoute.should.be.a.Function();
		ctx.hitchy.api.services.PolicyRoute.should.be.a.Function();
		ctx.hitchy.api.services.Router.should.be.a.Function();

		ctx.hitchy.api.config.routes["/foo"].should.be.a.Function();
	} );
} );
