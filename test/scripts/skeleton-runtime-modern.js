import { describe, before, after, it } from "mocha";
import "should";

import Core from "@hitchy/core/sdt.js";
import SDT from "../../index.js";

const Test = await SDT( Core );

const config = {
	projectFolder: false,
	files: {
		"config/routes.cjs": `exports.routes = { "/foo": ( _, res ) => res.send( "Hello!" ) };`,
	},
	options: {
		// debug: true,
	},
};

describe( "Testing with server-dev-tools faking project", () => {
	const ctx = {};

	before( Test.before( ctx, config ) );
	after( Test.after( ctx ) );

	it( "is supported", async() => {
		const response = await ctx.get( "/foo" );

		response.body.toString( "utf8" ).should.be.equal( "Hello!" );
	} );
} );
