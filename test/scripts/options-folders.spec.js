import Path from "node:path";
import { fileURLToPath } from "node:url";

import { describe, afterEach, it } from "mocha";
import "should";

import SDT from "../../index.js";

const __dirname = Path.resolve( fileURLToPath( import.meta.url ), ".." );
const Test = await SDT();

describe( "Options for picking folders", () => {
	/** @type ExtendedHitchyTestContext */
	const ctx = {};

	const localTestProjectFolder = Path.resolve( __dirname, "../project/basic" );
	const myRootFolder = Path.resolve( __dirname, "../.." );

	afterEach( Test.after( ctx ) );

	it( "are optional", async() => {
		await Test.before( ctx )();

		( ctx.hitchyOptions.temporaryFolder == null ).should.be.true();
		ctx.hitchyOptions.projectFolder.should.equal( localTestProjectFolder );
		( ctx.hitchyOptions.pluginsFolder == null ).should.be.true();

		ctx.hitchy.api.config.test.success.should.be.true();
	} );

	it( "accept `false` for projectFolder to disable its auto-discovery", async() => {
		await Test.before( ctx, { projectFolder: false, useTmpPath: true } )();

		( ctx.hitchyOptions.temporaryFolder == null ).should.be.true();
		ctx.hitchyOptions.projectFolder.should.not.equal( localTestProjectFolder );
		( ctx.hitchyOptions.pluginsFolder == null ).should.be.true();

		( ctx.hitchy.api.config.test == null ).should.be.true();
	} );

	it( "accept `false` for projectFolder as part of options to disable its auto-discovery", async() => {
		await Test.before( ctx, { options: { projectFolder: false }, useTmpPath: true } )();

		( ctx.hitchyOptions.temporaryFolder == null ).should.be.true();
		ctx.hitchyOptions.projectFolder.should.not.equal( localTestProjectFolder );
		( ctx.hitchyOptions.pluginsFolder == null ).should.be.true();

		( ctx.hitchy.api.config.test == null ).should.be.true();
	} );

	it( "fail setup if auto-detection is disabled without proper replacement", async() => {
		await Test.before( ctx, { projectFolder: false, useTmpPath: false } )().should.be.rejected();
		await Test.before( ctx, { projectFolder: false } )().should.be.rejected();

		await Test.before( ctx, { options: { projectFolder: false }, useTmpPath: false } )().should.be.rejected();
		await Test.before( ctx, { options: { projectFolder: false } } )().should.be.rejected();
	} );

	it( "accepts project folder as absolute path name", async() => {
		await Test.before( ctx, {
			projectFolder: localTestProjectFolder,
		} )();

		( ctx.hitchyOptions.temporaryFolder == null ).should.be.true();
		ctx.hitchyOptions.projectFolder.should.equal( localTestProjectFolder );
		( ctx.hitchyOptions.pluginsFolder == null ).should.be.true();

		ctx.hitchy.api.config.test.success.should.be.true();
	} );

	it( "accepts project folder relative to testing file's folder", async() => {
		await Test.before( ctx, {
			projectFolder: "../project/basic",
		} )();

		( ctx.hitchyOptions.temporaryFolder == null ).should.be.true();
		ctx.hitchyOptions.projectFolder.should.equal( localTestProjectFolder );
		( ctx.hitchyOptions.pluginsFolder == null ).should.be.true();

		ctx.hitchy.api.config.test.success.should.be.true();
	} );

	it( "accepts plugins folder as absolute path name", async() => {
		await Test.before( ctx, {
			pluginsFolder: myRootFolder,
		} )();

		( ctx.hitchyOptions.temporaryFolder == null ).should.be.true();
		ctx.hitchyOptions.projectFolder.should.equal( localTestProjectFolder );
		ctx.hitchyOptions.pluginsFolder.should.equal( myRootFolder );

		ctx.hitchy.api.config.test.success.should.be.true();
	} );

	it( "accepts plugins folder relative to testing file's folder", async() => {
		await Test.before( ctx, {
			pluginsFolder: "../..",
		} )();

		( ctx.hitchyOptions.temporaryFolder == null ).should.be.true();
		ctx.hitchyOptions.projectFolder.should.equal( localTestProjectFolder );
		ctx.hitchyOptions.pluginsFolder.should.equal( myRootFolder );

		ctx.hitchy.api.config.test.success.should.be.true();
	} );

	it( "accepts project folder and plugins folder as absolute path name", async() => {
		await Test.before( ctx, {
			projectFolder: localTestProjectFolder,
			pluginsFolder: myRootFolder,
		} )();

		( ctx.hitchyOptions.temporaryFolder == null ).should.be.true();
		ctx.hitchyOptions.projectFolder.should.equal( localTestProjectFolder );
		ctx.hitchyOptions.pluginsFolder.should.equal( myRootFolder );

		ctx.hitchy.api.config.test.success.should.be.true();
	} );

	it( "accepts project folder as absolute path name and plugins folder relative to testing file's folder", async() => {
		await Test.before( ctx, {
			projectFolder: localTestProjectFolder,
			pluginsFolder: "../..",
		} )();

		( ctx.hitchyOptions.temporaryFolder == null ).should.be.true();
		ctx.hitchyOptions.projectFolder.should.equal( localTestProjectFolder );
		ctx.hitchyOptions.pluginsFolder.should.equal( myRootFolder );

		ctx.hitchy.api.config.test.success.should.be.true();
	} );

	it( "accepts project folder relative to testing file's folder and plugins folder as absolute path name", async() => {
		await Test.before( ctx, {
			projectFolder: "../project/basic",
			pluginsFolder: myRootFolder,
		} )();

		( ctx.hitchyOptions.temporaryFolder == null ).should.be.true();
		ctx.hitchyOptions.projectFolder.should.equal( localTestProjectFolder );
		ctx.hitchyOptions.pluginsFolder.should.equal( myRootFolder );

		ctx.hitchy.api.config.test.success.should.be.true();
	} );

	it( "accepts project folder and plugins folder relative to testing file's folder", async() => {
		await Test.before( ctx, {
			projectFolder: "../project/basic",
			pluginsFolder: "../..",
		} )();

		( ctx.hitchyOptions.temporaryFolder == null ).should.be.true();
		ctx.hitchyOptions.projectFolder.should.equal( localTestProjectFolder );
		ctx.hitchyOptions.pluginsFolder.should.equal( myRootFolder );

		ctx.hitchy.api.config.test.success.should.be.true();
	} );
} );
