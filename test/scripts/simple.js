import { describe, before, after, it } from "mocha";
import "should";

import Core from "@hitchy/core/sdt.js";
import SDT from "../../index.js";

const Test = await SDT( Core );

describe( "SDT", () => {
	const ctx = {};

	before( Test.before( ctx ) );
	after( Test.after( ctx ) );

	it( "works without any custom configuration", async() => {
		const response = await ctx.get( "/foo" );

		response.statusCode.should.equal( 404 );
	} );
} );
