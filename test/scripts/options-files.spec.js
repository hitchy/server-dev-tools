import OS from "node:os";
import Path from "node:path";

import { describe, before, after, afterEach, it } from "mocha";
import FileEssentials from "file-essentials";
import "should";

import SDT from "../../index.js";

const Test = await SDT();

describe( "Injecting custom files", () => {
	const ctx = {};
	const tempBaseFolder = Path.resolve( OS.tmpdir(), "$hitchy-dev" );

	before( () => FileEssentials.rmdir( tempBaseFolder ) );
	afterEach( Test.after( ctx ) );
	after( () => FileEssentials.rmdir( tempBaseFolder ) );

	it( "creates temporary file in temporary folder solely based on provided `files` option", async() => {
		await Test.before( ctx, {
			projectFolder: false,
			files: {
				a: "// this is a test",
			}
		} )();

		// described file got created in a temporary folder
		const tempProjectFolder = ctx.temporaryFolder;
		const tempProjectFile = Path.resolve( tempProjectFolder, "a" );
		( await FileEssentials.read( tempProjectFile ) ).toString().should.eql( "// this is a test" );

		// it's the only file in that temporary project folder
		const files = await FileEssentials.find( tempProjectFolder );
		files.should.deepEqual( [ ".", "a", "package.json" /* written by SDT */ ] );

		// shut down server
		await Test.after( ctx )();

		// all temporary stuff gets removed on shutdown again
		await FileEssentials.stat( tempProjectFile ).should.be.resolvedWith( null );
		await FileEssentials.stat( tempProjectFolder ).should.be.resolvedWith( null );
	} );

	it( "passes temporary project folder as such into Hitchy", async() => {
		await Test.before( ctx, {
			files: {
				a: "// this is a test",
			}
		} )();

		ctx.temporaryFolder.should.be.equal( ctx.hitchyOptions.projectFolder );
	} );

	it( "creates file in a sub-folder providing configuration to be discovered by Hitchy", async() => {
		const content = `export default function( options ) {
	return {
		auth: {
			receivedProjectFolder: options.projectFolder,
			filterPassword: "truthy",
		}
	};
};`;

		await Test.before( ctx, {
			projectFolder: false,
			files: {
				"config/auth.js": content
			},
		} )();

		const tempProjectFolder = ctx.temporaryFolder;
		const configFile = Path.resolve( tempProjectFolder, "config/auth.js" );

		// configuration file should have been written properly
		( await FileEssentials.read( configFile ) ).toString().should.be.eql( content );

		// written configuration should be exposed in Hitchy's runtime API
		ctx.hitchy.api.config.auth.filterPassword.should.be.equal( "truthy" );
		ctx.hitchy.api.config.auth.receivedProjectFolder.should.be.equal( tempProjectFolder );

		// configuration file is only one in that temporary project folder
		const files = ( await FileEssentials.find( tempProjectFolder ) ).map( name => name.replaceAll( Path.sep, Path.posix.sep ) );
		files.should.deepEqual( [ ".", "config", "config/auth.js", "package.json" /* written by SDT */ ] );

		// shut down the server
		await Test.after( ctx )();

		// all temporary files and folders have been removed again
		await FileEssentials.stat( tempProjectFolder ).should.be.resolvedWith( null );
	} );

	it( "creates multiple files", async() => {
		const content1 = "export default {};";
		const content2 = `export default function( options ) {
	return {
		auth: {
			receivedProjectFolder: options.projectFolder,
			filterPassword: "truthy",
		}
	};
};`;

		await Test.before( ctx, {
			projectFolder: false,
			files: {
				"api/models/foo.js": content1,
				"config/auth.js": content2,
			},
		} )();

		const tempProjectFolder = ctx.temporaryFolder;
		const file1 = Path.resolve( tempProjectFolder, "api/models/foo.js" );
		const file2 = Path.resolve( tempProjectFolder, "config/auth.js" );

		// configuration file should have been written properly
		( await FileEssentials.read( file1 ) ).toString().should.be.eql( content1 );
		( await FileEssentials.read( file2 ) ).toString().should.be.eql( content2 );

		// written configuration should be exposed in Hitchy's runtime API
		ctx.hitchy.api.config.auth.filterPassword.should.be.equal( "truthy" );
		ctx.hitchy.api.config.auth.receivedProjectFolder.should.be.equal( tempProjectFolder );

		// configuration file is only one in that temporary project folder
		const files = ( await FileEssentials.find( tempProjectFolder ) ).map( file => file.replaceAll( Path.sep, Path.posix.sep ) );
		files.should.deepEqual( [ ".", "api", "api/models", "api/models/foo.js", "config", "config/auth.js", "package.json" /* written by SDT */ ] );

		// shut down the server
		await Test.after( ctx )();

		// all temporary files and folders have been removed again
		await FileEssentials.stat( tempProjectFolder ).should.be.resolvedWith( null );
	} );

	it( "can be combined with an existing project folder selected explicitly", async() => {
		await Test.before( ctx, {
			projectFolder: "../project/basic",
			files: {
				"config/extra.cjs": `module.exports = {
	extra: {
		success: true,
	}
};`,
			}
		} )();

		ctx.hitchy.api.config.test.success.should.be.true();
		ctx.hitchy.api.config.extra.success.should.be.true();
	} );

	it( "can use a combination of files, pluginsFolder and projectFolder", async() => {
		await Test.before( ctx, {
			projectFolder: "../project/basic",
			pluginsFolder: "../../",
			files: {
				"package.json": `{"type":"common"}`,
				"config/extra.js": `module.exports = {
	extra: {
		success: true,
	}
};`,
			}
		} )();

		ctx.hitchy.api.config.test.success.should.be.true();
		ctx.hitchy.api.config.extra.success.should.be.true();
	} );
} );
