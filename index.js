import OS from "node:os";
import Path from "node:path";
import File from "node:fs";
import Crypto from "node:crypto";
import { parse, fileURLToPath } from "node:url";
import { createRequire } from "node:module";
import Http from "node:http";
import Child from "node:child_process";
import { promisify } from "node:util";

import FileEssentials from "file-essentials";
import PromiseEssentials from "promise-essentials";
import Debug from "debug";

const require = createRequire( import.meta.url );
const temporaryBaseFolder = Path.resolve( OS.tmpdir(), "$hitchy-dev" );

const logDebug = Debug( "hitchy:server-dev-tools:debug" );
const logError = Debug( "hitchy:server-dev-tools:error" );

const sdtName = "@hitchy/server-dev-tools";

const ignoreSubs = [ "coverage", "dist", "node_modules", "public", ".git", ".idea", ".vitepress", ".vscode", ".vuepress" ];

const __filename = fileURLToPath( import.meta.url );

const exec = promisify( Child.exec );

const DefaultArguments = {
	quiet: true,
};


/**
 * Detects if Hitchy instance is meant to integrate with Express.js or not.
 *
 * @param {object} args CLI arguments to pass into Hitchy instance
 * @returns {boolean} true if Hitchy is going to be integrated with Express.js
 */
const useExpress = args => {
	switch ( args?.injector || process.env.HITCHY_MODE || "node" ) {
		case "connect" :
		case "express" :
			return true;
	}

	return false;
};

/**
 * Generates Server Dev Tools API associated with explicitly provided SDT API of
 * @hitchy/core using according API of @hitchy/core found as dependency as
 * fallback.
 *
 * @param {Hitchy.ServerDevTools.CoreAPI} core minimum API of @hitchy/core for running an instance for testing
 * @returns {Promise<Hitchy.ServerDevTools.API>} promise for API to run tests on instances in context of provided @hitchy/core
 */
export default async function( core = undefined ) {
	if ( !core ) {
		core = ( await import( "@hitchy/core/sdt.js" ) ).default;
	}

	if ( parseInt( core.version ) !== 1 ) {
		throw new TypeError( `unsupported v${core.version} of @hitchy/core` );
	}

	/**
	 * Starts hitchy service using node's http server.
	 *
	 * @param {object} options global options
	 * @param {object} args arguments as supported by Hitchy's CLI tool
	 * @returns {Promise<{server:Server, hitchy:Hitchy.Core.Instance}>} promises running server exposing hitchy instance
	 */
	const startServer = async( options = {}, args = {} ) => {
		if ( !options.logger ) {
			core.Logger.reset();
		}


		const _args = Object.assign( {}, DefaultArguments, args );

		// we need to mock some CLI behavior here for the CLI isn't involved on
		// starting Hitchy via SDT
		if ( _args.debug ) {
			options.debug = true;
		}

		if ( options.debug ) {
			_args["log-level"] = "*";
		} else if ( _args.quiet && !_args["log-level"] ) {
			_args["log-level"] = "-*";
		} else {
			_args["log-level"] = core.LogLevels[_args["log-level"] || "INFO"] || _args["log-level"];
		}

		core.Logger.update( _args["log-level"] );

		if ( !_args.hasOwnProperty( "port" ) ) {
			_args.port = "auto";
		}


		if ( useExpress( args ) ) {
			await checkExpress();
		}

		return core.BasicServer( options, _args, () => {
			if ( options.debug ) {
				console.error( "Hitchy has been shut down." );

				dumpHandles();
			}
		} );
	};

	const compileOptions = ( ctx, configuration, customFolders ) => {
		const options = Object.assign( { arguments: {} }, configuration.options, customFolders );

		if ( options.logger ) {
			ctx.logged = []; // eslint-disable-line no-param-reassign

			if ( options.logger === true ) {
				core.Logger.replace( new core.CapturingLogger( ( namespace, message ) => {
					ctx.logged?.push( ( /\b(error|alert|critical)\b/i.test( namespace ) ? "ERROR: " : "LOG: " ) + message );
				} ) );
			} else if ( options.logger instanceof core.AbstractLogger ) {
				core.Logger.replace( options.logger );
			} else {
				throw new TypeError( "invalid logger replacement in configuration" );
			}

			ctx.hitchyReplacedErrorLogger = console.error; // eslint-disable-line no-param-reassign
			console.error = core.Logger.get( "error" );

			ctx.hitchyReplacedLogLogger = console.log; // eslint-disable-line no-param-reassign
			console.log = core.Logger.get( "log" );
		}

		return options;
	};

	/**
	 * Generates function for setting up Hitchy for testing.
	 *
	 * @param {HitchyTestContext} ctx test context to be established
	 * @param {Hitchy.ServerDevTools.Configuration} configuration test runner options
	 * @param {number} timeout custom timeout in milliseconds to wait for the server to finish booting up
	 * @returns {function(): Promise<{server: Server, hitchy: Hitchy.Core.Instance}>} function for use with test runner to set up Hitchy for testing
	 */
	function before( ctx, configuration = {}, timeout = 2000 ) {
		ctx.hitchy = null; // eslint-disable-line no-param-reassign
		ctx.server = null; // eslint-disable-line no-param-reassign

		const callerFile = getCallerFile();

		return async function() {
			if ( this && typeof this.timeout === "function" ) {
				const minimum = useExpress( configuration.options?.arguments || [] ) ? 10000 : 0;

				this.timeout( Math.max( minimum, timeout ) );
			}

			const { customFolders, temporaryFolder } = await qualifyConfiguration( configuration, callerFile );
			const options = compileOptions( ctx, configuration, customFolders );
			const { hitchy, server } = await startServer( options, options.arguments );

			return qualifyContextDescriptor( hitchy, server, ctx, options, temporaryFolder );
		};
	}

	/**
	 * Generates function for tearing down some Hitchy previously set up for testing.
	 *
	 * @param {HitchyTestContext} ctx test context to be established
	 * @param {boolean} keepFiles set true to prevent removal of temporary project folder
	 * @returns {function(): Promise} function for use with test runner to tear down Hitchy after testing
	 */
	function after( ctx, keepFiles = false ) {
		return async function() {
			try {
				if ( ctx.hitchy ) {
					await ctx.hitchy.api.shutdown();
				}
			} catch ( cause ) {
				if ( !cause.intentionalCrash ) {
					throw cause;
				}
			} finally {
				if ( ctx.logged ) {
					console.error = ctx.hitchyReplacedErrorLogger;
					ctx.loggerError = null; // eslint-disable-line no-param-reassign

					console.log = ctx.hitchyReplacedLogLogger;
					ctx.loggerLog = null; // eslint-disable-line no-param-reassign
				}

				/* eslint-disable no-param-reassign */
				ctx.server = null;
				ctx.hitchy = null;
				ctx.logged = null;
				/* eslint-enable no-param-reassign */
			}

			await postStop( ctx, keepFiles );
		};
	}

	/**
	 * Starts Hitchy server exposing project in provided folder and loading plugins
	 * from explicitly listed folders.
	 *
	 * @param {Hitchy.ServerDevTools.Configuration} configuration customizes behaviour of invoked Hitchy server
	 * @returns {Promise<Hitchy.ServerDevTools.ContextInformation>} promises started server instance of Hitchy
	 */
	async function start( configuration = {} ) {
		const ctx = {};

		const { customFolders, temporaryFolder } = await qualifyConfiguration( configuration );
		const options = compileOptions( ctx, configuration, customFolders );
		const { hitchy, server } = await startServer( options, options.arguments );

		return qualifyContextDescriptor( hitchy, server, ctx, options, temporaryFolder );
	}

	/**
	 * Stops Hitchy server.
	 *
	 * @param {Hitchy.ServerDevTools.RuntimeContext} context promise for Hitchy server started or that server itself
	 * @param {boolean} keepFiles set true to prevent removal of temporary project folder
	 * @returns {Promise<any>} promises Hitchy server stopped
	 */
	async function stop( context, keepFiles = false ) {
		const ctx = await context;

		if ( ctx?.hitchy ) {
			await ctx.hitchy.api.shutdown();
		}

		await postStop( ctx, keepFiles );
	}


	return {
		before,
		after,
		start,
		stop,
	};
}



/**
 * Dumps active handles of current Node.js process to stderr.
 *
 * @returns {void}
 */
function dumpHandles() {
	// FIXME replace wtfnode package with natively supported methods
	//  process.getActiveResourcesInfo() that has been introduced in more recent
	//  versions of node.js
	const handles = process._getActiveHandles();

	if ( handles.length < 1 ) {
		return;
	}

	console.error( `${new Date().toISOString()}: dump of active handles:` );

	handles.forEach( handle => {
		if ( handle && handle._events ) {
			Object.keys( handle._events )
				.map( name => {
					const handlers = handle._events[name];
					const _handlers = Array.isArray( handlers ) ? handlers : [handlers];
					const num = _handlers.filter( handler => typeof handler === "function" ).length;

					if ( num > 0 ) {
						console.error( `${num} handler(s) listening for ${name} event of ${( handle.constructor || {} ).name} %j`, handle );
					}
				} );
		}
	} );
}

/**
 * Assures [express](https://expressjs.com/) to be available.
 *
 * @returns {Promise} promises express being installed
 */
async function checkExpress() {
	try {
		await import( "express" );
	} catch ( cause ) {
		switch ( cause.code ) {
			case "MODULE_NOT_FOUND" :
			case "ERR_MODULE_NOT_FOUND" :
				break;

			default :
				throw cause;
		}

		console.log( "`express` is missing, thus will be installed now" );

		await exec( "npm install --no-save express" );

		await import( "express" );
	}
}

/**
 * Implements tools for common tasks in developing tests.
 */

/**
 * @typedef {function(url:string, body:(Buffer|string|object), headers:object):Promise<ServerResponse>} HitchyTestBoundClient
 */

/**
 * @typedef {function(method:string, url:string, body:(Buffer|string|object), headers:object):Promise<ServerResponse>} HitchyTestUnboundClient
 */

/**
 * @typedef {object} HitchyTestContext
 * @property {Hitchy.Core.Instance} hitchy instance of Hitchy to be tested
 * @property {Server} server HTTP service dispatching incoming requests into Hitchy instance
 * @property {string} logged list of captured lög messages if enabled in options
 * @property {HitchyTestBoundClient} get sends GET request to running Hitchy instance
 * @property {HitchyTestBoundClient} post sends POST request to running Hitchy instance
 * @property {HitchyTestBoundClient} put sends PUT request to running Hitchy instance
 * @property {HitchyTestBoundClient} patch sends PATCH request to running Hitchy instance
 * @property {HitchyTestBoundClient} delete sends DELETE request to running Hitchy instance
 * @property {HitchyTestBoundClient} head sends HEAD request to running Hitchy instance
 * @property {HitchyTestBoundClient} options sends OPTIONS request to running Hitchy instance
 * @property {HitchyTestBoundClient} trace sends TRACE request to running Hitchy instance
 * @property {HitchyTestUnboundClient} request sends custom request to running Hitchy instance
 */

/**
 * Sends HTTP request to hitchy server, receives responds and fulfills returned
 * promise with response on success or with cause on error.
 *
 * @param {string} method HTTP method
 * @param {string} url requested URL
 * @param {(Buffer|string|object)=} data data to be sent with request
 * @param {Object<string,string>} headers custom headers to include on request
 * @returns {Promise} promises response
 */
export function request( method, url, data = null, headers = {} ) {
	const promise = new Promise( ( resolve, reject ) => {
		const server = this && this.server;
		if ( !server ) {
			throw new Error( "server not started yet" );
		}

		const serverUrl = `http://127.0.0.1:${server.address().port}`;
		const requestUrl = new URL( url, serverUrl );

		const req = {
			agent: false,
			method: method,
			protocol: requestUrl.protocol,
			hostname: requestUrl.hostname,
			port: requestUrl.port,
			path: requestUrl.pathname + requestUrl.search,
			headers: {
				accept: "text/html",
			},
		};

		for ( const name of Object.keys( headers || {} ) ) {
			req.headers[name] = headers[name];
		}

		let body = data;
		if ( typeof body !== "string" && !Buffer.isBuffer( body ) && body != null ) {
			body = JSON.stringify( body );
			req.headers["content-type"] = "application/json; charset=UTF-8";
		}

		const handle = Http.request( req, response => {
			const buffers = [];

			response.on( "data", chunk => buffers.push( chunk ) );
			response.once( "end", () => {
				response.body = Buffer.concat( buffers ); // eslint-disable-line no-param-reassign

				const type = response.headers["content-type"] || "";

				if ( type.match( /^(text|application)\/json\b/ ) ) {
					try {
						response.data = JSON.parse( response.body.toString( "utf8" ) ); // eslint-disable-line no-param-reassign
					} catch ( e ) {
						reject( e );
					}
				} else if ( type.match( /^text\// ) ) {
					response.text = response.body.toString( "utf8" ); // eslint-disable-line no-param-reassign
				}

				resolve( response );
			} );
		} );

		process.nextTick( () => { promise.request = handle; } );

		handle.on( "error", reject );

		if ( body != null ) {
			handle.write( body, "utf8" );
		}

		handle.end();
	} );

	return promise;
}

/**
 * Synchronously tests if selected file/folder exists.
 *
 * @param {string} file path name of file/folder to check
 * @returns {Stats} stat on found file/folder, falsy if file/folder is missing
 * @throws {Error} if checking file/folder caused unexpected issue
 */
function exists( file ) {
	try {
		return File.statSync( file );
	} catch ( cause ) {
		if ( cause.code === "ENOENT" ) {
			return undefined;
		}

		throw cause;
	}
}

/**
 * Extracts name of file calling some local code from error stack frame.
 *
 * @return {string} path name of caller's file
 */
function getCallerFile() {
	const lines = new Error().stack.split( /\r?\n/ );

	for ( const line of lines ) {
		const match = /^\s*at\s+\S+\s+\(\s*(.+):\d+:\d+\s*\)\s*/.exec( line );

		if ( match ) {
			const filename = match[1].startsWith( "file://" ) ? fileURLToPath( match[1] ) : match[1];

			if ( filename !== __filename ) {
				return filename;
			}
		}
	}

	return undefined;
}

/**
 * Ascends from provided path name looking for folder containing package.json
 * file but no hitchy.json file (unless configured to test a hitchy plugin)
 * with the former naming server-dev-tools as dependency.
 *
 * @param {string} start path name of folder to start ascending from
 * @param {boolean} forTestingPlugin if true, project folder is searched for testing a plugin (thus accepting a hitchy.json file, too)
 * @returns {string} path name of found containing folder, undefined if no folder was found
 */
function ascendToProjectRootFolder( start, forTestingPlugin ) {
	const candidates = [];
	let path = start;

	// collect path names of containing folders with package.json files looking
	// for the one declaring SDT as dependency
	while ( path ) {
		const file = Path.resolve( path, "package.json" );

		if ( exists( file ) ) {
			candidates.push( file );

			const { name, dependencies = {}, devDependencies = {} } = require( file ) || {};

			if ( devDependencies[sdtName] || dependencies[sdtName] || name === sdtName ) {
				if ( forTestingPlugin || !exists( Path.resolve( path, "hitchy.json" ) ) ) {
					break;
				}
			}
		}

		const next = Path.resolve( path, ".." );
		path = next === path ? undefined : next;
	}

	if ( candidates.length === 1 ) {
		// found single match -> use right away
		return Path.dirname( candidates[0] );
	}

	if ( candidates.length > 1 ) {
		// found multiple ... check farthest one for workspace definition
		// covering closer ones and use either one based on the result
		const farthestFile = candidates.pop();
		const farthestFolder = Path.dirname( farthestFile );
		const { workspaces } = require( farthestFile ) || {};

		for ( const pattern of workspaces || [] ) {
			const fullPattern = Path.resolve( farthestFolder, pattern );

			const regexp = new RegExp( "^" + fullPattern.replace( /\/\*\*\/|\*|\?|[\\/|?+.(){}[\]]/g, match => {
				switch ( match ) {
					case "/" : return "\\/";
					case "/!**!/" : return "\\/(?:.{1,64}\\/)?";
					case "*" : return "[^/]{1,64}";
					case "?" : return "[^/]";
					default : return "\\" + match;
				}
			} ) + "$" );

			for ( const candidate of candidates ) {
				const folder = Path.dirname( candidate );

				if ( regexp.test( folder ) ) {
					return folder;
				}
			}
		}

		return farthestFolder;
	}

	return undefined;
}


/**
 * Ascends from selected folder to closest parent containing a package.json file
 * and returns its full path name on success.
 *
 * @param {string} folder path name of folder to start ascending from
 * @returns {string} path name of found containing folder, undefined if no folder was found
 */
function ascendToClosestPackageJson( folder ) {
	while ( folder ) {
		const file = Path.resolve( folder, "package.json" );

		if ( exists( file ) ) {
			return file;
		}

		const next = Path.resolve( folder, ".." );
		folder = next === folder ? undefined : next;
	}

	return undefined;
}

/**
 * Ascends from provided path name looking for level containing hitchy.json
 * file indicating root of a plugin.
 *
 * @param {string} start path name of folder to start ascending from
 * @returns {string} path name of found containing folder, undefined if no folder was found
 */
function ascendToPluginRootFolder( start ) {
	let path = start;

	while ( path ) {
		if ( exists( Path.resolve( path, "hitchy.json" ) ) ) {
			return path;
		}

		if ( exists( Path.resolve( path, "package.json" ) ) ) {
			break;
		}

		const next = Path.resolve( path, ".." );
		path = next === path ? undefined : next;
	}

	return undefined;
}

/**
 * Searches provided folder for subordinated folder resembling a hitchy server
 * implementation.
 *
 * @param {string} containerFolder path name of folder containing hitchy server implementation
 * @param {string} testScriptFolder path name of folder containing file implementing test script using server dev tools
 * @param {boolean} forTestingPlugin if true, the folder is searched for testing a hitchy plugin instead of an application
 * @returns {Promise<string>} found folder, undefined if no folder was found
 */
async function descendToProjectCodeFolder( containerFolder, testScriptFolder, forTestingPlugin ) {
	const ignore = [ ...ignoreSubs, "data" ];
	const tests = [
		[ "api/controller", "api/controllers" ],
		[ "api/policy", "api/policies" ],
		[ "api/model", "api/models" ],
		[ "api/service", "api/services" ],
		["config"],
	];

	const candidates = await FileEssentials.find( containerFolder, {
		qualified: true,
		filterSelf: true,
		minDepth: forTestingPlugin ? 1 : 0,
		maxDepth: 10,
		filter: ( _, full, stat ) => stat.isDirectory() && ignore.indexOf( Path.basename( full ) ) < 0,
	} );

	const ptnSkipFolder = /^(?:\.|node_modules)/;
	const ptnCodeFile = /(?<!\.spec|\.test)\.[mc]?js$/;
	const found = [];

	for ( const candidate of candidates ) {
		nextCandidate: // eslint-disable-line no-labels
			for ( const test of tests ) {
				for ( const sub of test ) {
					const path = Path.resolve( candidate, sub );
					const stat = exists( path );

					if ( stat && stat.isDirectory() ) {
						const codeFiles = await FileEssentials.find( path, { // eslint-disable-line no-await-in-loop
							maxDepth: 3,
							filter: ( name, _, _stat ) => ( _stat.isDirectory() ? !ptnSkipFolder.test( name ) : ptnCodeFile.test( name ) ),
							converter: ( name, _, _stat ) => ( _stat.isFile() ? name : undefined ),
						} );

						if ( codeFiles.length > 0 ) {
							found.push( candidate );
							break nextCandidate; // eslint-disable-line no-labels
						}
					}
				}
			}
	}

	if ( found.length === 1 ) {
		return found[0];
	}

	logDebug( "auto-discovering project folder: multiple candidates", found );

	return undefined;
}

/**
 * Commonly qualifies context descriptor after having started Hitchy successfully.
 *
 * @param {Hitchy.Core.Instance} hitchy Hitchy instance attached to created HTTP server
 * @param {Server} server HTTP server Hitchy instance is attached to
 * @param {?Hitchy.ServerDevTools.ContextDescriptor} context context descriptor to use instead of Hitchy-provided one
 * @param {Hitchy.ServerDevTools.Configuration} options provided toolkit options
 * @param {?string} temporaryFolder path name of optionally used temporary folder
 * @returns {Hitchy.ServerDevTools.ContextDescriptor} qualified context descriptor
 */
function qualifyContextDescriptor( hitchy, server, context, options, temporaryFolder ) {
	/* eslint-disable no-param-reassign */
	context.hitchy = hitchy;
	context.server = server;

	context.get = ( url, headers ) => request.call( context, "GET", url, null, headers );
	context.post = ( url, body, headers ) => request.call( context, "POST", url, body, headers );
	context.put = ( url, body, headers ) => request.call( context, "PUT", url, body, headers );
	context.patch = ( url, body, headers ) => request.call( context, "PATCH", url, body, headers );
	context.delete = ( url, body, headers ) => request.call( context, "DELETE", url, body, headers );
	context.head = ( url, headers ) => request.call( context, "HEAD", url, null, headers );
	context.options = ( url, headers ) => request.call( context, "OPTIONS", url, null, headers );
	context.trace = ( url, headers ) => request.call( context, "TRACE", url, null, headers );

	context.request = request.bind( context );

	context.hitchyOptions = options;

	if ( temporaryFolder ) {
		context.temporaryFolder = temporaryFolder;
	}
	/* eslint-enable no-param-reassign */

	return context;
}

/**
 * @typedef {object} Folders
 * @prop {string} temporaryFolder
 * @prop {{pluginsFolder: string, projectFolder: string, explicitPlugins: string[]}} customFolders
 */

/**
 * Processes certain options specific to server-dev-tools in preparation for
 * starting Hitchy instance.
 *
 * @param {Hitchy.ServerDevTools.Configuration} configuration customizes behaviour of server dev tools running Hitchy
 * @param {string} callerFile path name of file containing originally calling code to use instead of auto-discovered one
 * @returns {Promise<Folders>} promises information resulting from preparing start of server
 */
async function qualifyConfiguration( configuration = {}, callerFile = undefined ) {
	const {
		options = {},
		plugin = false,
		files = null
	} = configuration;
	let {
		useTmpPath = false,
	} = configuration;
	let projectFolder = options.projectFolder ?? configuration.projectFolder;
	const result = {
		customFolders: {},
	};
	let { pluginsFolder = undefined } = configuration;


	Debug.enable( `hitchy:server-dev-tools:*${options.debug ? "" : ",-hitchy:server-dev-tools:debug"}` );

	logDebug( "Node.js versions: %j", process.versions );


	// get the path name of the folder containing the file with the calling test script
	const testScriptFolder = Path.resolve( Path.dirname( callerFile || getCallerFile() ) );

	logDebug( `used in ${testScriptFolder}` );


	// get the root folder of the project containing that test script
	const rootFolder = ascendToProjectRootFolder( testScriptFolder, plugin );
	if ( !rootFolder ) {
		throw new Error( "discovering folder of project containing current test script has failed" );
	}


	if ( projectFolder && typeof projectFolder === "string" ) {
		// optionally qualify relative project folder provided in configuration
		if ( projectFolder.startsWith( "./" ) || projectFolder.startsWith( "../" ) ) {
			// --> relative to calling file's folder
			projectFolder = Path.resolve( testScriptFolder, projectFolder );
		} else if ( !Path.isAbsolute( projectFolder ) ) {
			// --> relative to root folder of project containing the test script
			projectFolder = Path.resolve( rootFolder, projectFolder );
		}
	} else if ( projectFolder == null ) {
		// auto-discover project folder
		logDebug( `auto-discover project folder for ${testScriptFolder}` );

		projectFolder = await descendToProjectCodeFolder( rootFolder, testScriptFolder, plugin );
	}

	if ( projectFolder && typeof projectFolder === "string" ) {
		logDebug( `project folder is ${projectFolder}` );
	} else if ( plugin ) {
		logDebug( "auto-discovering project folder failed, assuming empty project folder ... do not use npm link or provide projectFolder option explicitly" );

		useTmpPath = true;
	} else if ( projectFolder === false ) {
		logDebug( "auto-discovering project folder disabled" );
	} else {
		logError( "auto-discovering project folder failed ... do not use npm link or provide projectFolder option explicitly" );
	}


	if ( pluginsFolder && typeof pluginsFolder === "string" && pluginsFolder !== "$temp" ) {
		// optionally qualify relative path name of folder containing plugin(s) to be discovered explicitly
		if ( /^\.\.?(?:\/.*)?$/.test( pluginsFolder ) ) {
			// --> relative to calling file's folder
			pluginsFolder = Path.resolve( testScriptFolder, pluginsFolder );
		} else if ( !Path.isAbsolute( pluginsFolder ) ) {
			if ( projectFolder && typeof projectFolder === "string" ) {
				// --> relative to root folder of project containing the test script
				pluginsFolder = Path.resolve( projectFolder, pluginsFolder );
			} else {
				throw new Error( "pluginsFolder has been configured in relation to projectFolder, but the latter is unknown" );
			}
		}
	} else if ( pluginsFolder == null ) {
		// discover plugins folder
		logDebug( `auto-discover plugins folder for ${projectFolder || testScriptFolder}` );

		if ( plugin ) {
			pluginsFolder = ascendToPluginRootFolder( testScriptFolder );
		}

		if ( pluginsFolder === projectFolder ) {
			logDebug( `no dedicated plugin folder found, plugin discovery will be limited to project folder` );
			pluginsFolder = undefined;
		} else if ( pluginsFolder ) {
			logDebug( `plugins folder is ${pluginsFolder}` );
		} else {
			logDebug( `no dedicated plugins folder has been discovered` );
		}
	}

	return new Promise( ( resolve, reject ) => {
		if ( files || useTmpPath ) {
			if ( projectFolder ) {
				logDebug( `copying files from ${projectFolder} into temporary folder ... this might take a while` );
			}

			FileEssentials.mkdir( temporaryBaseFolder )
				.then( () => {
					// create random temporary folder just for this test run
					const baseFolder = Path.resolve( temporaryBaseFolder, Crypto.randomBytes( 8 ).toString( "hex" ) );

					result.customFolders.projectFolder = result.temporaryFolder = baseFolder;

					return FileEssentials.mkdir( baseFolder );
				} )
				.then( async() => {
					logDebug( `temporary project folder is ${result.temporaryFolder}` );

					const packageJsonFile = ascendToClosestPackageJson( projectFolder || testScriptFolder );
					if ( packageJsonFile ) {
						const meta = JSON.parse( await File.promises.readFile( packageJsonFile, { encoding: "utf-8" } ) );

						if ( meta.type ) {
							const fallbackFile = Path.resolve( result.temporaryFolder, "package.json" );

							logDebug( `writing fallback file ${fallbackFile} to declare type ${meta.type}` );

							await File.promises.writeFile( fallbackFile, JSON.stringify( { type: meta.type } ), { encoding: "utf-8" } );
						}
					}

					// copy files from provided template folder to temporary one
					if ( !projectFolder ) {
						return undefined;
					}

					return FileEssentials.find( projectFolder, {
						filter: local => !ignoreSubs.includes( local ),
						converter: ( local, _, stat ) => ( stat.isFile() ? local : null ),
					} )
						.then( sources => PromiseEssentials.each( sources, source => {
							return FileEssentials.read( Path.resolve( projectFolder, source ) )
								.then( content => {
									return FileEssentials.mkdir( result.temporaryFolder, Path.dirname( source ) )
										.then( () => FileEssentials.write( Path.resolve( result.temporaryFolder, source ), content ) )
										.then( () => {
											logDebug( `- copied ${source}` );
										} );
								} );
						} ) );
				} )
				.then( () => {
					// write any additional files into temporary folder
					if ( !files ) {
						return undefined;
					}

					return PromiseEssentials.each( Object.keys( files ), key => {
						return FileEssentials.mkdir( Path.resolve( result.temporaryFolder, Path.dirname( key ) ) )
							.then( () => FileEssentials.write( Path.resolve( result.temporaryFolder, key ), files[key] ) );
					} );
				} )
				.then( () => resolve( result ) )
				.catch( err => {
					logError( err );
					reject( err );
				} );
		} else {
			if ( !projectFolder ) {
				throw new TypeError( "missing required select of folder containing some Hitchy project" );
			}

			result.customFolders.projectFolder = String( projectFolder );

			resolve( result );
		}
	} )
		.then( _result => {
			if ( pluginsFolder ) {
				const strPluginsFolder = String( pluginsFolder );

				_result.customFolders.pluginsFolder = strPluginsFolder;
				_result.customFolders.explicitPlugins = [strPluginsFolder];
			}

			return _result;
		} );
}

/**
 * Cleans up after Hitchy has been stopped.
 *
 * @param {Hitchy.ServerDevTools.ContextInformation} context context of running tests
 * @param {boolean} keepFiles set true to prevent code from removing temporary project folder
 * @returns {Promise|undefined} optional promise for having cleaned up
 */
async function postStop( context, keepFiles = false ) {
	if ( context.temporaryFolder && !keepFiles ) {
		const { temporaryFolder } = context;

		await FileEssentials.rmdir( temporaryFolder );

		const baseFolder = Path.resolve( temporaryBaseFolder );
		const stats = await FileEssentials.stat( baseFolder );

		if ( stats && stats.isDirectory() ) {
			const list = await FileEssentials.list( baseFolder );

			if ( list.length ) {
				await FileEssentials.rmdir( baseFolder );
			}
		}
	}
}
